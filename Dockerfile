FROM registry.gitlab.com/dedyms/node:16-dev AS tukang
RUN apt update && \
    apt install -y --no-install-recommends python3-dev python-is-python3 && npm -g install pnpm
USER $CONTAINERUSER
RUN git clone --depth=1 https://github.com/diogonr/detsube.git $HOME/detsube
WORKDIR $HOME/detsube
RUN pnpm -d install

FROM registry.gitlab.com/dedyms/ffmpeg-static AS ffmpeg

FROM registry.gitlab.com/dedyms/node:16
RUN apt update && apt install -y --no-install-recommends python3-minimal python-is-python3 libfontconfig1 && apt clean && rm -rf /var/lib/apt/lists/* && npm -g install pnpm
COPY --chown=$CONTAINERUSER:$CONTAINERUSER --from=tukang $HOME/detsube $HOME/detsube
COPY --chown=$CONTAINERUSER:$CONTAINERUSER --from=ffmpeg $HOME/.local $HOME/.local
WORKDIR $HOME/detsube
USER $CONTAINERUSER
VOLUME $HOME/detsube/db
CMD ["pnpm","run","start:dev"]